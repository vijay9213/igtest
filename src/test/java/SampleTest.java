import org.junit.Assert;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class SampleTest {

    public static Response test() {
        RestAssured.baseURI = "http://demo4032024.mockable.io/apitest";
        RequestSpecification requestSpecification = RestAssured.given();
        Response response = requestSpecification.get();
        return response;
    }

    @Test
    public void validateStatusCode() {
        Response response = test();
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200);
    }

    @Test
    public void validateHeader() {
        Response response = test();
        String contentType = response.header("Content-Type");
        Assert.assertEquals(contentType, "application/json; charset=UTF-8");
    }

    @Test
    public void validateResponseBody() {
        Response response = test();
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200);
        JsonPath jsonPathEvaluator = response.jsonPath();

        int age = jsonPathEvaluator.get("employeeData.age[0]");
        Assert.assertEquals(age, 25);
        String role = jsonPathEvaluator.get("employeeData.role[0]");
        Assert.assertEquals(role, "QA Automation Developer");
        String Dob = jsonPathEvaluator.get("employeeData.dob[0]");
        Assert.assertEquals(Dob, "25-02-1994");
        String message = jsonPathEvaluator.get("message");
        Assert.assertEquals(message, "data retrieved successful");
    }

    @Test
    public void validateCompany() {
        Response response = test();
        JsonPath jsonPathEvaluator = response.jsonPath();
        String company = jsonPathEvaluator.get("employeeData.company[0]");
        Assert.assertEquals(company, "ABC Infotech");
    }


}
